import React from 'react'
import './App.css'
import AliceCarousel from 'react-alice-carousel'
import "react-alice-carousel/lib/alice-carousel.css"
import slide1 from './img/slide1.jpg'
import slide2 from './img/slide2.jpg'
import slide3 from './img/slide3.jpg'
import slide4 from './img/slide4.jpg'

export default function App(){
  return(
<div className='App'>
<AliceCarousel autoPlay autoPlayInterval="3000">
  <img src={slide1} className='sliderimg' />
  <img src={slide2} className='sliderimg' />
  <img src={slide3} className='sliderimg' />
  <img src={slide4} className='sliderimg' />
</AliceCarousel>
</div>
  )
}